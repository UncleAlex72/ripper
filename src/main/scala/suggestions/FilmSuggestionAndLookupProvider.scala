package suggestions

import com.typesafe.scalalogging.StrictLogging
import io.circe._

import java.net.{URL, URLEncoder}
import java.time.Year
import io.circe.generic.extras._
import io.circe.syntax._
import io.circe.generic.auto._
import scala.io.Source

/**
  * Created by alex on 19/06/16.
  */
trait FilmSuggestionAndLookupProvider extends SuggestionProvider with LookupProvider with StrictLogging {

  implicit val configuration: Configuration = Configuration.default

  @ConfiguredJsonCodec case class SearchResult(title: String, @JsonKey("release_date") year: Option[Year], id: Int)
  @ConfiguredJsonCodec case class Response(page: Int, @JsonKey("results") searchResults: Seq[SearchResult])

  object SearchResult {
    implicit val yearDecoder: Decoder[Option[Year]] = Decoder[String].map { date =>
      val releaseDate = """(\d{4})-\d{2}-\d{2}""".r
      date match {
        case releaseDate(year) => Some(Year.of(year.toInt))
        case _ => None
      }
    }
  }

  val tmdbApiKey = "3061ab25346f16fc3bb5f1e7da3a4b68"

  def search(text: String, year: Option[Int] = None): Seq[SearchResult] = {
    val parameters = Seq(
      "api_key" -> tmdbApiKey,
      "search_type" -> "ngram",
      "query" -> text,
      "include_adult" -> true) ++
      year.map(year => "year" -> year)
    val url = "http://api.themoviedb.org/3/search/movie?" +
      parameters.map(p => s"${p._1}=${URLEncoder.encode(p._2.toString)}").mkString("&")
    val source = Source.fromURL(new URL(url))
    try {
      parser.decode[Response](source.mkString) match {
        case Right(response) => response.searchResults
        case Left(err) =>
          logger.error(s"Could not read from url [$url]", err)
          Seq.empty
      }
    } finally {
      source.close()
    }
  }

  override def suggestions(text: String): Seq[String] = {
    search(text).map { result =>
      (Seq(result.title) ++ result.year.map(year => s"($year)")).mkString(" ")
    }
  }

  override def lookupUrl(name: String): Option[URL] = {
    val nameAndYear = """(.+?)(?: \((\d{4})\))?""".r
    val (text, year) = name match {
      case nameAndYear(title, year) => (title, Option(year).map(_.toInt))
    }
    val searchResults = search(text, year)
    searchResults.headOption.map(searchResult => new URL(s"https://www.themoviedb.org/movie/${searchResult.id}"))
  }

}
