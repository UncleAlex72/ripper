package gui

import commands.Film
import suggestions.FilmSuggestionAndLookupProvider

/**
  * Created by alex on 14/05/15.
  */
class FilmRipGui extends AbstractDvdGui("Film DVD Ripper") with FilmDvdGui with RipGui[FilmTitle, Film] with FilmSuggestionAndLookupProvider
