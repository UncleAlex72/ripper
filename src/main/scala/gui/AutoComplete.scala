package gui

import javafx.util.Callback
import org.controlsfx.control.textfield.AutoCompletionBinding.ISuggestionRequest
import org.controlsfx.control.textfield.{AutoCompletionBinding, TextFields}
import scalafx.scene.control.TextField

import java.util
import scala.jdk.CollectionConverters._

/**
 * Created by alex on 17/05/15.
 */
object AutoComplete {

  implicit class AutoCompleteTextField(val textField: TextField) {

    def autoComplete(minLength: Int, suggestionProvider: String => Seq[String]): TextField = {
      val callback = new Callback[AutoCompletionBinding.ISuggestionRequest, util.Collection[String]] {
        override def call(param: ISuggestionRequest): util.Collection[String] = {
          val text = param.getUserText
          if (text.length >= minLength && !param.isCancelled) {
            suggestionProvider(text)
          }
          else {
            Seq.empty[String]
          }
        }.asJava
      }
      TextFields.bindAutoCompletion(textField, callback)
      textField
    }
  }
}
