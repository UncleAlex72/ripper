package commands.makemkv

import commands.FilenameSanitiser
import org.scalatest.matchers.must.Matchers
import org.scalatest.wordspec.AnyWordSpec


/**
 * Created by alex on 07/05/15.
 */
class FilenameSanitiserSpec extends AnyWordSpec with FilenameSanitiser with Matchers {

  "Upper case characters" should {
    "be replaced by lower case characters" in {
      sanitise("AB") must ===("ab")
    }
  }

  "Lower case characters" should {
    "stay the same" in {
      sanitise("AB") must ===("ab")
    }
  }

  "Digits" should {
    "stay the same" in {
      sanitise("543") must ===("543")
    }
  }

  "Punctuation" should {
    "be removed" in {
      sanitise("!,()") must ===("")
    }
  }

  "Whitespace" should {
    "be normalised to a single underscore" in {
      sanitise("ab \tcd") must ===("ab_cd")
    }
  }
}
