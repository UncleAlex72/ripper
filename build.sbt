name := """ripper"""

version := "1.0-SNAPSHOT"

scalaVersion := "2.13.8"

scalacOptions += "-Ymacro-annotations"
javacOptions ++= Seq("-source", "1.8", "-target", "1.8", "-Xlint")

libraryDependencies ++= Seq(
  "org.scalatest" %% "scalatest" % "3.2.12" % "test",
  "org.scalatestplus" %% "mockito-4-5" % "3.2.12.0" % "test",
  "joda-time" % "joda-time" % "2.7",
  "org.scalafx" %% "scalafx" % "16.0.0-R24",
  "org.controlsfx" % "controlsfx" % "11.1.1",
  "org.scala-lang.modules" %% "scala-xml" % "2.1.0",
  "com.typesafe.scala-logging" %% "scala-logging" % "3.9.4",
  "ch.qos.logback" % "logback-classic" % "1.2.11"
) ++ Seq("base", "controls", "fxml", "graphics", "media", "swing", "web").map(m => "org.openjfx" % s"javafx-$m" % "16" classifier "linux")++ Seq(
  "io.circe" %% "circe-core",
  "io.circe" %% "circe-generic",
  "io.circe" %% "circe-generic-extras",
  "io.circe" %% "circe-parser"
  ).map(_ % "0.14.1")

mainClass in run in Compile := Some("gui.GuiRunner")
